import QtQuick 2.0

Item {
  property Item colors: colors
  Item {
    id: colors
    property color accent_purple: "#a84eaf" //Qt.rgba(0.658, 0.305, 0.686) #a84eaf
    property color accent_cyan: "#7baefb"
    property color border_color: "#7a7a95"
  }
}
