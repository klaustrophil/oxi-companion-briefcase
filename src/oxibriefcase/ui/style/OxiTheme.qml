pragma Singleton
import QtQuick 2.12

QtObject {

  property QtObject colors: QtObject {
    property color accent_purple: "#a84eaf" //Qt.rgba(0.658, 0.305, 0.686) #a84eaf
    property color accent_cyan: "#7baefb"
    property color border_color: "#7a7a95"
    property color font_color: "#ffffff"


  }

}

